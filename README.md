 Project was created to play around with OpenGL libraries.

  Folder "external" contains standart libraries, such as GLEW, GLFW, GLM etc.

  Folder "shaders" has description of classic shaders

  Folder "common" contains custom classes made for different purposes:
 ---- bsp_tree        -   Contains XY coordinates for the walls and instruction how to initialise BSP Tree and draw GameObjects of the walls which get into the Field of View
 ---- camera_controls -   Describes WASD + Mouse controls and calculation of Projection Matrix
 ---- GameObject      -   Initialises world coordinates of the object, generates Vertices/UVs/Normals and draws objects in GL
 ---- objloader      -   Loads .obj file
 ---- shader          -   Contains instruction how to use shader
 ---- shapeGenerator  -   Generates cube and simple terrain
 ---- stb_image.h     -   Library to load images
 ---- tangentspace    -   Used for Normal Mapping
 ---- terrain         -   Terrain generator
 ---- texture         -   Load texture from image
 ---- tga             -   load TGA image
 ---- vboindexer      -   Index vertices to triangles  
