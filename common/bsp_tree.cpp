
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLM
#include <glm/glm.hpp>

#include "camera_controls.hpp"
#include "GameObject.h"
#include "bsp_tree.h"


//std::vector<glm::vec2> list_of_walls = {
//        glm::vec2(-2.0, -3.0), glm::vec2(0.0, -4.0),
//        glm::vec2(-3.0, -5.0), glm::vec2(-2.0, -3.0),
//        glm::vec2(-5.0, -1.0), glm::vec2(-3.0, -5.0),
//        glm::vec2(0.0, -4.0), glm::vec2(-2.0, -6.0),
//        glm::vec2(-2.0, -6.0), glm::vec2(4.0, -6.0),
//        glm::vec2(4.0, -6.0), glm::vec2(4.0, -2.0),
//        glm::vec2(4.0, -2.0), glm::vec2(2.0, -1.0),
//        glm::vec2(2.0, -1.0), glm::vec2(2.0, 2.0),
//        glm::vec2(2.0, 2.0), glm::vec2(4.0, 4.0)
//};

BSPnode * root;

// Wall's Heights
float Y_MIN = -2.0f;
float Y_MAX = -6.0f;


/* ----------------------- Building BSP Tree ----------------------  */

BSPnode * buildBSPTree(const std::vector<glm::vec2> & list_of_walls)
{
    printf("----- buildBSPTree: -------\n");

    for (int i = 0; i < list_of_walls.size(); i +=2)
    {
        printf("new wall: (%2.1f, %2.1f) - (%2.1f, %2.1f)\n",
               list_of_walls[i].x, list_of_walls[i].y, list_of_walls[i + 1].x, list_of_walls[i + 1].y);

        insert(list_of_walls[i], list_of_walls[i + 1]);
        printf("\n");
    }

    return root;

}



void insert(glm::vec2 p1, glm::vec2 p2)
{
    if (root != NULL) {
        insert(root, p1, p2);
    }
    else
    {
        root = new BSPnode;
        root -> parent = NULL;
        root -> front_child = NULL;
        root -> back_child = NULL;
        root -> vertex1 = p1;
        root -> vertex2 = p2;
        root -> front_normal = countFrontNormal(p1, p2);
        root -> back_normal = countBackNormal(p1, p2);

        printf("To Root: (%2.1f, %2.1f) - (%2.1f, %2.1f)\n",
               root->vertex1.x, root->vertex1.y, root->vertex2.x, root->vertex2.y);
    }
}


void insert(BSPnode * leaf, glm::vec2 p1, glm::vec2 p2)
{
    printf(" (%2.1f, %2.1f)-(%2.1f, %2.1f)",
           leaf->vertex1.x, leaf->vertex1.y, leaf->vertex2.x, leaf->vertex2.y);

    switch (checkRelation(leaf, p1, p2))
    {
        case IN_FRONT or SAME_LINE:

            printf(" -> ");

            if (leaf -> front_child != NULL) {

                insert(leaf->front_child, p1, p2);
            }
            else {

                BSPnode * childNode = new BSPnode;

                leaf->front_child = childNode;
                childNode->parent = leaf;
                childNode->front_child = NULL;
                childNode->back_child = NULL;
                childNode->vertex1 = p1;
                childNode->vertex2 = p2;
                childNode->front_normal = countFrontNormal(p1, p2);
                childNode->back_normal = countBackNormal(p1, p2);

                printf("To Front: (%2.1f, %2.1f) - (%2.1f, %2.1f)\n", p1.x, p1.y, p2.x, p2.y);
            }

            break;

        case IN_BACK:

            printf(" <- ");

            if (leaf -> back_child != NULL) {

                insert(leaf->back_child, p1, p2);
            }
            else {
                BSPnode * childNode = new BSPnode;

                leaf->back_child = childNode;
                childNode->parent = leaf;
                childNode->front_child = NULL;
                childNode->back_child = NULL;
                childNode->vertex1 = p1;
                childNode->vertex2 = p2;
                childNode->front_normal = countFrontNormal(p1, p2);
                childNode->back_normal = countBackNormal(p1, p2);


                printf("To Back: (%2.1f, %2.1f)-(%2.1f, %2.1f)\n", p1.x, p1.y, p2.x, p2.y);

            }

            break;

        case INTERSECTS:

            printf("\nintersection at leaf (%2.1f, %2.1f) - (%2.1f, %2.1f): ",
                   leaf->vertex1.x, leaf->vertex1.y, leaf->vertex2.x, leaf->vertex2.y);

            glm::vec2 front_p1, front_p2, back_p1, back_p2;

     /* ----- Split the wall and add one part to the front branch and another to the back branch */
            splitWall(leaf, p1, p2, front_p1, front_p2, back_p1, back_p2);

            printf(" Wall is cutted into: (%2.1f, %2.1f)-(%2.1f, %2.1f) X (%2.1f, %2.1f)-(%2.1f, %2.1f)\n",
                   front_p1.x, front_p1.y, front_p2.x, front_p2.y, back_p1.x, back_p1.y, back_p2.x, back_p2.y);

     /* --------------- Add first half to the front ---------- */
            if (leaf->front_child != NULL){

                insert(leaf->front_child, front_p1, front_p2);
            }
            else {

                BSPnode * childNode = new BSPnode;

                leaf->front_child = childNode;
                childNode->parent = leaf;
                childNode->front_child = NULL;
                childNode->back_child = NULL;
                childNode->vertex1 = front_p1;
                childNode->vertex2 = front_p2;
                childNode->front_normal = countFrontNormal(front_p1, front_p2);
                childNode->back_normal = countBackNormal(front_p1, front_p2);

                printf("To Front: (%2.1f, %2.1f)-(%2.1f, %2.1f)\n", front_p1.x, front_p1.y, front_p2.x, front_p2.y);
            }

     /* --------------- Add second half to the back ---------- */
            if (leaf->back_child != NULL) {

                insert(leaf->back_child, back_p1, back_p2);
            }
            else {
                BSPnode * childNode = new BSPnode;

                leaf->back_child = childNode;
                childNode->parent = leaf;
                childNode->front_child = NULL;
                childNode->back_child = NULL;
                childNode->vertex1 = back_p1;
                childNode->vertex2 = back_p2;
                childNode->front_normal = countFrontNormal(back_p1, back_p1);
                childNode->back_normal = countBackNormal(back_p2, back_p2);


                printf("To Back: (%2.1f, %2.1f) - (%2.1f, %2.1f)\n", back_p1.x, back_p1.y, back_p2.x, back_p2.y);

            }

            break;

    }
}




glm::vec2 countFrontNormal(glm::vec2 v1, glm::vec2 v2)
{
    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;

    return glm::normalize( glm::vec2( -dy, dx) );
}


glm::vec2 countBackNormal(glm::vec2 v1, glm::vec2 v2)
{
    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;

    return glm::normalize( glm::vec2( dy, -dx) );
}



/* check if the child node wall IN_FRONT, IN_BACK, INTERSECTS or at the SAME_LINE
 * with the parent's infinitely extended wall */
int checkRelation(BSPnode *leaf, glm::vec2 p1, glm::vec2 p2)
{
    const float &leafX1 = leaf->vertex1.x;
    const float &leafX2 = leaf->vertex2.x;
    const float &leafY1 = leaf->vertex1.y;
    const float &leafY2 = leaf->vertex2.y;
    float dx = leafX2 - leafX1;
    float dy = leafY2 - leafY1;

    float sideP1 = (p1.x - leafX1) * dy - (p1.y - leafY1) * dx;
    float sideP2 = (p2.x - leafX1) * dy - (p2.y - leafY1) * dx;

    if (sideP1 == 0 && sideP2 == 0 )
        return SAME_LINE;
    if (sideP1 <= 0 && sideP2 <= 0 )
        return IN_FRONT;
    if (sideP1 >= 0 && sideP2 >= 0 )
        return IN_BACK;
    if (sideP1 >= 0 && sideP2 <= 0 || sideP1 <= 0 && sideP2 >= 0)
        return INTERSECTS;

    printf("Could not define the IN_FRONT, IN_BACK, INTERSECTS or SAME_LINE relation between nodes in BSP Tree\n");
    return -1;
}





void splitWall(BSPnode * leaf, glm::vec2 & p1, glm::vec2 & p2,
               glm::vec2 & front_p1, glm::vec2 & front_p2,
               glm::vec2 & back_p1, glm::vec2 & back_p2)
{

    const float &leafX1 = leaf->vertex1.x;
    const float &leafX2 = leaf->vertex2.x;
    const float &leafY1 = leaf->vertex1.y;
    const float &leafY2 = leaf->vertex2.y;
    float dx = leafX2 - leafX1;
    float dy = leafY2 - leafY1;

    float sideP1 = (p1.x - leafX1) * dy - (p1.y - leafY1) * dx;    // sideN < 0 then point N in_front
    float sideP2 = (p2.x - leafX1) * dy - (p2.y - leafY1) * dx;    // sideN > 0 then point N in_back

    float m1 = dy / dx;
    float m2 = (p2.y - p1.y) / (p2.x - p1.x);

    float x, y; // intersection point
    x = (m1 * leafX1 - leafY1 - m2 * p1.x + p1.y) / (m1 - m2);
    y = m1 * (x - leafX1) + leafY1;   // or y = m2 * (x - p1.x) + p1.y;

//    printf("INTERSECTION point is P(%2.1f, %2.1f)\n", x, y);

    if (sideP1 <= 0 && sideP2 >= 0)
    {
        front_p1 = p1;
        front_p2.x = x;
        front_p2.y = y;

        back_p1.x = x;
        back_p1.y = y;
        back_p2 = p2;

        return;
    }
    else if (sideP1 >= 0 && sideP2 <= 0)
    {
        front_p1.x = x;
        front_p1.y = y;
        front_p2 = p2;

        back_p1 = p1;
        back_p2.x = x;
        back_p2.y = y;

        return;
    }

    printf("bsp_tree.cpp -> void splitWall(...) made a mistake :/ probably two lines are parallel to each other");
    return;
}





/* ----------- Functions to find borders of the camera view angle --------------- */
/*
 * ProjectionMatrix[0][0] = 2 * zNear / (right - left) is the right and left side clipping border
 * also Proj[0][0] = 1 / tan (Theta) = Adjacent / Opposite,
 * where Opposite is X axis and Adjacent is Z axis in ModelView coordinates
 * x' = ModelView * x
 * z' = ModelView * z
 *
 * thus z' = x' * Proj[0][0]
 *
 * */

glm::vec4 getLeftSightVector()
{
    glm::mat4 ViewMatrix = getViewMatrix();
    glm::mat4 ProjectionMatrix = getProjectionMatrix();

    glm::vec4 xAxisPoint = ViewMatrix * glm::vec4(-1.0f, 0.0f, 0.0f, 1.0f);

    float x = xAxisPoint.x;
    float z = ProjectionMatrix[0][0] * x;

    glm::vec4 leftSightBorderPoint = glm::vec4(x, 0.0f, z, 1.0);
    glm::vec4 playerPosition = getCameraPosition();

    return leftSightBorderPoint - playerPosition;
}

glm::vec4 getRightSightVector()
{
    glm::mat4 ViewMatrix = getViewMatrix();
    glm::mat4 ProjectionMatrix = getProjectionMatrix();

    glm::vec4 xAxisPoint = ViewMatrix * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

    float x = xAxisPoint.x;
    float z = ProjectionMatrix[0][0] * x;

    glm::vec4 rightSightBorderPoint = glm::vec4(x, 0.0f, z, 1.0);
    glm::vec4 playerPosition = getCameraPosition();

    return rightSightBorderPoint - playerPosition;
}


/* ----------------------- Traversing Back BSP Tree ----------------------  */


/*
 * ----------------- Render BSP Tree ---------------------
 * */

void renderBSP(BSPnode *node, GLuint textureID, GLuint programID)
{
    glm::vec4 camera_position = getCameraPosition();
    int relation = checkRelation(node, camera_position);

    if (relation == IN_FRONT)
    {
        if (node->back_child != NULL) renderBSP(node->back_child, textureID, programID);
        drawFrontWall(node->vertex1, node->vertex2, node->back_normal, textureID, programID);
        if (node->front_child != NULL) renderBSP(node->front_child, textureID, programID);
    }
    else
    {
        if (node->front_child != NULL) renderBSP(node->front_child, textureID, programID);
        drawBackWall(node->vertex1, node->vertex2, node->front_normal, textureID, programID);
        if (node->back_child != NULL) renderBSP(node->back_child, textureID, programID);
    }

    return;
}

/*  Check if point P is IN_FRONT, IN_BACK or ON_PLANE of the leaf plane */
int checkRelation(BSPnode *leaf, glm::vec4 P)
{
    if (leaf == NULL)
    {
        printf("Could Not checkRelation() between BSPnode and player position: *node == NULL");
        return -1;
    }

    glm::vec3 L = glm::vec3(leaf->vertex1.x, 0.0f, leaf->vertex1.y);
    glm::vec3 LP = glm::vec3(P.x - L.x, P.y - L.y, P.z - L.z);

//    glm::vec3 FN = glm::vec3(leaf->front_normal.x, 0.0f, leaf->front_normal.y);
//    float dot = glm::dot(LP, FN);
    /* !!! had to work like this but it didn't, so I simplified the equations
     * dot product of two vectors a and b:   a.b = ax * bx + ay * by + az * bz
     * but front_normal.y is always 0.0, so
     * LP.FN = LP.x * FN.x + LP.z * FN.z
     * */
    float d = LP.x * leaf->front_normal.x + LP.z * leaf->front_normal.y;


    /* Use EPSILON instead of 0.0 due to the floating point specifics */
    if (d < -EPSILON )
        return IN_FRONT;
    if (d > EPSILON )
        return IN_BACK;

    return ON_PLANE;

}



/*
 * ----------------- Draw a Wall ---------------------
 * */
void drawFrontWall(glm::vec2 p1, glm::vec2 p2, glm::vec2 normal, GLuint TextureID, GLuint programID)
{

    std::vector<glm::vec3> vertices = {
            glm::vec3(p1.x, Y_MIN, p1.y),
            glm::vec3(p2.x, Y_MIN, p2.y),
            glm::vec3(p2.x, Y_MAX, p2.y),
            glm::vec3(p1.x, Y_MAX, p1.y)
    };

    std::vector<glm::vec2> uvs = {
            glm::vec2(0.0f, 0.0f),
            glm::vec2(1.0f, 0.0f),
            glm::vec2(1.0f, 1.0f),
            glm::vec2(0.0f, 1.0f)
    };

    glm::vec3 n = glm::vec3(normal.x, 0.0f, normal.y);
    std::vector<glm::vec3> normals = {
            n, n, n, n
    };

    std::vector<unsigned short> indices = {
            0, 1, 2,
            0, 2, 3
//            0, 2, 1,
//            0, 3, 2
    };

//    printf("Drawing wall:\n");
//    for (int i = 0; i < vertices.size(); i++)
//    {
//        printf("vertex[%d]: (%2.1f, %2.1f, %2.1f)\n", i,
//               vertices[i].x, vertices[i].y, vertices[i].z);
//    }
//    printf("normal is: (%2.1f, %2.1f, %2.1f)\n", normals[0].x, normals[0].y, normals[0].z);
//
//    printf("\n");


    GameObject wall = GameObject(vertices, uvs, normals, indices);
    wall.setShader(programID);
    wall.setTexture(TextureID);

    vertices.clear();
    uvs.clear();
    normals.clear();
    indices.clear();


//    glFrontFace(GL_CCW);
    wall.update();
//    glFrontFace(GL_CCW);

}

/* different order of indexing: Clockwise indexing for Counter-Clockwise OpenGL setteng */
void drawBackWall(glm::vec2 p1, glm::vec2 p2, glm::vec2 normal, GLuint TextureID, GLuint programID)
{

    std::vector<glm::vec3> vertices = {
            glm::vec3(p1.x, Y_MIN, p1.y),
            glm::vec3(p2.x, Y_MIN, p2.y),
            glm::vec3(p2.x, Y_MAX, p2.y),
            glm::vec3(p1.x, Y_MAX, p1.y)
    };

    std::vector<glm::vec2> uvs = {
            glm::vec2(0.0f, 0.0f),
            glm::vec2(1.0f, 0.0f),
            glm::vec2(1.0f, 1.0f),
            glm::vec2(0.0f, 1.0f)
    };

    glm::vec3 n = glm::vec3(normal.x, 0.0f, normal.y);
    std::vector<glm::vec3> normals = {
            n, n, n, n
    };

    std::vector<unsigned short> indices = {
            0, 2, 1,
            0, 3, 2
//            0, 1, 2,
//            0, 2, 3
    };


    GameObject wall = GameObject(vertices, uvs, normals, indices);
    wall.setShader(programID);
    wall.setTexture(TextureID);

    vertices.clear();
    uvs.clear();
    normals.clear();
    indices.clear();



//    glFrontFace(GL_CCW);
    wall.update();
//    glFrontFace(GL_CCW);

}