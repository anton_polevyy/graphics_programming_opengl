#ifndef CONTROLS_HPP
#define CONTROLS_HPP

void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();
glm::mat4 calculateProjectionMatrix(float FoV, float aspectRatio, float zNear, float zFar);

glm::vec4 getCameraPosition();


#endif
