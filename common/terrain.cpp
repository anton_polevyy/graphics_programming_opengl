
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <stdint.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Include TGA image loader
#include "tga.h"

#include "terrain.h"

static int terrainGridWidth ,terrainGridLength;
//static float *terrainHeights = NULL;
std::vector<float> terrainHeights;
std::vector<glm::vec4> terrainColorsRGBA;
std::vector<glm::vec3> terrainVertices;
std::vector<unsigned short> terrainIndices;
std::vector<glm::vec2> terrainUVs;
std::vector<glm::vec3> terrainNormals;
glm::vec3 vertex;
float xFrom = -14.0f, xTo = 14.0f;
float zFrom = -14.0f, zTo = 14.0f;
float yFrom = -13.0f, yTo = -3.0f;

int generateTerrainFromTGA(char * imagepath, std::vector<glm::vec3> & vertices,
                           std::vector<glm::vec2> & uvs,
                           std::vector<glm::vec3> & normals,
                           std::vector<unsigned short> & indices)
{
    getHeightsFromTGA(imagepath);
    terrainComputeVertices(xFrom, xTo, zFrom, zTo, yFrom, yTo);
    terrainComputeIndices();
    terrainComputeUVs();
//    terrainComputeUVsAsTiles();
    terrainComputeNormals();

    printf("terrainHeights.size() = %d\n", terrainHeights.size());
    printf("terrainIndices.size() = %d\n", terrainIndices.size());

//    for (int i = 0 ; i < terrainGridLength; i++)
//        for (int j = 0;j < terrainGridWidth; j++)
//        {
//            printf("terrainVertices[%d, %d] = (%f, %f, %f)\n", j, i,
//                   terrainVertices[i*terrainGridWidth + j].x,
//                   terrainVertices[i*terrainGridWidth + j].y,
//                   terrainVertices[i*terrainGridWidth + j].z);
//        }


//        for (int j = 0; j < terrainIndices.size() - 6; j += 6)
//        {
//            printf("Triangle %d: \n", j/3);
//            printf(" (%f, %f, %f) ",
//                   terrainVertices[terrainIndices[j]].x,
//                   terrainVertices[terrainIndices[j]].y,
//                   terrainVertices[terrainIndices[j]].z);
//            printf(" (%f, %f, %f) ",
//                   terrainVertices[terrainIndices[j + 1]].x,
//                   terrainVertices[terrainIndices[j + 1]].y,
//                   terrainVertices[terrainIndices[j + 1]].z);
//            printf(" (%f, %f, %f) \n",
//                   terrainVertices[terrainIndices[j + 2]].x,
//                   terrainVertices[terrainIndices[j + 2]].y,
//                   terrainVertices[terrainIndices[j + 2]].z);
//
//            printf("Triangle %d: \n", (j + 3)/3);
//            printf(" (%f, %f, %f) ",
//                   terrainVertices[terrainIndices[j + 3]].x,
//                   terrainVertices[terrainIndices[j + 3]].y,
//                   terrainVertices[terrainIndices[j + 3]].z);
//            printf(" (%f, %f, %f) ",
//                   terrainVertices[terrainIndices[j + 4]].x,
//                   terrainVertices[terrainIndices[j + 4]].y,
//                   terrainVertices[terrainIndices[j + 4]].z);
//            printf(" (%f, %f, %f) \n\n",
//                   terrainVertices[terrainIndices[j + 5]].x,
//                   terrainVertices[terrainIndices[j + 5]].y,
//                   terrainVertices[terrainIndices[j + 5]].z);
//        }

//    for (int j = 0; j < terrainIndices.size() - 6; j += 6)
//    {
//        printf("Triangle %d: \n", j/3);
//        printf(" (%f, %f) ",
//               terrainUVs[terrainIndices[j]].x,
//               terrainUVs[terrainIndices[j]].y);
//        printf(" (%f, %f) ",
//               terrainUVs[terrainIndices[j + 1]].x,
//               terrainUVs[terrainIndices[j + 1]].y);
//        printf(" (%f, %f) \n",
//               terrainUVs[terrainIndices[j + 2]].x,
//               terrainUVs[terrainIndices[j + 2]].y);
//
//        printf("Triangle %d: \n", (j + 3)/3);
//        printf(" (%f, %f) ",
//               terrainUVs[terrainIndices[j + 3]].x,
//               terrainUVs[terrainIndices[j + 3]].y);
//        printf(" (%f, %f) ",
//               terrainUVs[terrainIndices[j + 4]].x,
//               terrainUVs[terrainIndices[j + 4]].y);
//        printf(" (%f, %f) \n\n",
//               terrainUVs[terrainIndices[j + 5]].x,
//               terrainUVs[terrainIndices[j + 5]].y);
//    }


    // Clear vectors in case they are not empty
    vertices.clear();
    uvs.clear();
    normals.clear();
    indices.clear();


    // Copy generated terrainVertices to the vector std::vector<float> vertices
    copy(&terrainVertices[0], &terrainVertices[terrainVertices.size()], back_inserter(vertices));

    // Copy generated terrainUVs to the vector std::vector<float> vertices
    copy(&terrainUVs[0], &terrainUVs[terrainUVs.size()], back_inserter(uvs));

    // Copy generated terrainNormals to the vector std::vector<float> vertices
    copy(&terrainNormals[0], &terrainNormals[terrainNormals.size()], back_inserter(normals));

    // Copy generated terrainVertices to the vector std::vector<float> vertices
    copy(&terrainIndices[0], &terrainIndices[terrainIndices.size()], back_inserter(indices));

//    printf("\nterrainVertices.size() = %d, vertices.size() = %d\n", terrainVertices.size(), vertices.size());

    // Free up vectors of data
    terrainVertices.clear();
    terrainUVs.clear();
    terrainNormals.clear();
    terrainIndices.clear();
}







/* ----------------- Compute Heights from TGA Functions ----------------------- */

int getHeightsFromTGA(char * imagepath)
{
    tgaInfo *info;
    int mode;
    float pointHeight;


    /* load the image, using the tgalib */
    info = tgaLoad(imagepath);

    /* check to see if the image was properly loaded
        remember: only greyscale, RGB or RGBA noncompressed images */
    if (info->status != TGA_OK)
        return(TERRAIN_ERROR_LOADING_IMAGE);

    /* if the image is RGB, convert it to greyscale
        mode will store the image's number of components */
    mode = info->pixelDepth / 8;
    if (mode == 3) {
        tgaRGBtoGreyscale(info);
        mode = 1;
    }


    /* set the width and height of the terrain */
    terrainGridWidth = info->width;
    terrainGridLength = info->height;

//    terrainGridWidth = 14;
//    terrainGridLength = 14;

//    /* alocate memory for the terrain, and check for errors */
//    terrainHeights = (float *)malloc(terrainGridWidth * terrainGridLength * sizeof(float));
//    if (terrainHeights == NULL)
//        return(TERRAIN_ERROR_MEMORY_PROBLEM);

    /* if mode = RGBA then allocate memory for colors, and check for errors */
    if (mode == 4) {
        terrainColorsRGBA.reserve(terrainGridWidth * terrainGridLength);
        printf("Image has RGBA format\n");

    }


    /* fill arrays */
    for (int i = 0 ; i < terrainGridLength; i++)
        for (int j = 0;j < terrainGridWidth; j++) {

            /* compute the height as a value between 0.0 and 1.0 */
            pointHeight = info->imageData[mode*(i*terrainGridWidth + j)+(mode-1)] / 255.0;
//            terrainHeights[i*terrainGridWidth + j] = pointHeight;
//            pointHeight = 0.0f;
            terrainHeights.push_back(pointHeight);

//            printf("y color = %f\n", pointHeight);


            /* if mode = RGBA then fill the colors array as well */
            if (mode==4) {

                terrainColorsRGBA[i*terrainGridWidth + j].r = (info->imageData[mode*(i*terrainGridWidth + j)])/255.0;
                terrainColorsRGBA[i*terrainGridWidth + j].g = (info->imageData[mode*(i*terrainGridWidth + j + 1)])/255.0;
                terrainColorsRGBA[i*terrainGridWidth + j].b = (info->imageData[mode*(i*terrainGridWidth + j + 2)])/255.0;
                terrainColorsRGBA[i*terrainGridWidth + j].a = (info->imageData[mode*(i*terrainGridWidth + j + 3)])/255.0;


            }
        }

/* free the image's memory  */
    tgaDestroy(info);

    return(TERRAIN_OK);
}


//int getHeightsFromTGA(char * imagepath)
//{
//    tgaInfo *info;
//    int mode;
//    float pointHeight;
//
//
//    /* load the image, using the tgalib */
//    info = tgaLoad(imagepath);
//
//    /* check to see if the image was properly loaded
//        remember: only greyscale, RGB or RGBA noncompressed images */
//    if (info->status != TGA_OK)
//        return(TERRAIN_ERROR_LOADING_IMAGE);
//
//    /* if the image is RGB, convert it to greyscale
//        mode will store the image's number of components */
//    mode = info->pixelDepth / 8;
//    if (mode == 3) {
//        tgaRGBtoGreyscale(info);
//        mode = 1;
//    }
//
//
//    /* set the width and height of the terrain */
//    terrainGridWidth = info->width;
//    terrainGridLength = info->height;
//
//    /* alocate memory for the terrain, and check for errors */
//    terrainHeights = (float *)malloc(terrainGridWidth * terrainGridLength * sizeof(float));
//    if (terrainHeights == NULL)
//        return(TERRAIN_ERROR_MEMORY_PROBLEM);
//
//    /* if mode = RGBA then allocate memory for colors, and check for errors */
//    if (mode == 4) {
//        terrainColorsRGBA.reserve(terrainGridWidth * terrainGridLength);
//        printf("Image has RGBA format\n");
//
//    }
//
//
//    /* fill arrays */
//    for (int i = 0 ; i < terrainGridLength; i++)
//        for (int j = 0;j < terrainGridWidth; j++) {
//
//            /* compute the height as a value between 0.0 and 1.0 */
//            pointHeight = info->imageData[mode*(i*terrainGridWidth + j)+(mode-1)] / 255.0;
//            terrainHeights[i*terrainGridWidth + j] = pointHeight;
////            terHeights.push_back(pointHeight);
//
//
//            /* if mode = RGBA then fill the colors array as well */
//            if (mode==4) {
//
//                terrainColorsRGBA[i*terrainGridWidth + j].r = (info->imageData[mode*(i*terrainGridWidth + j)])/255.0;
//                terrainColorsRGBA[i*terrainGridWidth + j].g = (info->imageData[mode*(i*terrainGridWidth + j + 1)])/255.0;
//                terrainColorsRGBA[i*terrainGridWidth + j].b = (info->imageData[mode*(i*terrainGridWidth + j + 2)])/255.0;
//                terrainColorsRGBA[i*terrainGridWidth + j].a = (info->imageData[mode*(i*terrainGridWidth + j + 3)])/255.0;
//
//
//            }
//        }
//
///* free the image's memory  */
//    tgaDestroy(info);
//
//    return(TERRAIN_OK);
//}



void terrainComputeVertices(float xFrom, float xTo, float zFrom, float zTo, float yFrom, float yTo)
{
    float dx = (xTo - xFrom) / terrainGridWidth;
    float dz = (zTo - zFrom) / terrainGridLength;
    float dHeight = yTo - yFrom;


    for (int i = 0 ; i < terrainGridLength; i++)
        for (int j = 0;j < terrainGridWidth; j++)
        {
            vertex.x = xFrom + j * dx;
            vertex.z = zFrom + i * dz;
            vertex.y = yFrom + terrainHeights[i * terrainGridWidth + j] * dHeight;

            terrainVertices.push_back(vertex);
        }

//    for (int i = 0 ; i < terrainGridLength; i++)
//        for (int j = 0;j < terrainGridWidth; j++)
//        {
//            printf("terrainVertices[%d, %d] = (%f, %f, %f)\n", j, i,
//                   terrainVertices[i*terrainGridWidth + j].x,
//                   terrainVertices[i*terrainGridWidth + j].y,
//                   terrainVertices[i*terrainGridWidth + j].z);
//        }



}


void terrainComputeIndices()
{
    /* Make indices vector */
    int v1, v2, v3, v4, v5, v6;

    for (int i = 0 ; i < terrainGridLength - 1; i++)
        for (int j = 0;j < terrainGridWidth - 1; j++)
        {

            v1 = i * terrainGridWidth + j;
            v2 = i * terrainGridWidth + j + 1;
            v3 = (i + 1) * terrainGridWidth + j + 1;
            v4 = (i + 1) * terrainGridWidth + j;

//            /* 1st triangle in the quad */
//            v1 = i * terrainGridWidth + j;
//            v2 = i * terrainGridWidth + j + 1;
//            v3 = (i + 1) * terrainGridWidth + j + 1;
//
//            /* 2nd triangle in the quad */
//            v4 = (i + 1) * terrainGridWidth + j + 1;
//            v5 = (i + 1) * terrainGridWidth + j;
//            v6 = i * terrainGridWidth + j;

            /* push them to the vector of indices */
//            terrainIndices.push_back(v1);
//            terrainIndices.push_back(v2);
//            terrainIndices.push_back(v3);
//            terrainIndices.push_back(v4);
//            terrainIndices.push_back(v5);
//            terrainIndices.push_back(v6);

            /* 1st triangle in the quad */
            terrainIndices.push_back(v1);
            terrainIndices.push_back(v3);
            terrainIndices.push_back(v2);

            /* 2nd triangle in the quad */
            terrainIndices.push_back(v1);
            terrainIndices.push_back(v4);
            terrainIndices.push_back(v3);
        }

}






/* ----------------- Compute UVs Functions ----------------------- */
void terrainComputeUVs()
{

    for (int i = 0 ; i < terrainGridLength; i ++)
        for (int j = 0;j < terrainGridWidth; j ++)
        {
            terrainUVs.push_back(glm::vec2( i / (float) terrainGridLength, j / (float)terrainGridWidth ) );
        }
}


void terrainComputeUVsAsTiles()
{
    for (int i = 0; i < terrainGridLength * terrainGridWidth; i++)
        terrainUVs.push_back(glm::vec2(0.0f));


    glm::vec2 uv1 = glm::vec2(0.0f, 0.0f);
    glm::vec2 uv2 = glm::vec2(1.0f, 0.0f);
    glm::vec2 uv3 = glm::vec2(1.0f, 1.0f);
    glm::vec2 uv4 = glm::vec2(0.0f, 1.0f);

    for (int i = 0 ; i < terrainGridLength - 1; i += 2)
        for (int j = 0;j < terrainGridWidth - 1; j += 2)
        {
            terrainUVs[i * terrainGridWidth + j] = uv1;
            terrainUVs[i * terrainGridWidth + j + 1] = uv2;
            terrainUVs[(i + 1) * terrainGridWidth + j] = uv3;
            terrainUVs[(i + 1) * terrainGridWidth + j + 1] = uv4;
        }
}



/* ----------------- Compute Normals Functions ----------------------- */

void terrainComputeNormals()
{
    for (int i = 0 ; i < terrainGridWidth * terrainGridLength; i++)
        terrainNormals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
}


// /* Triangles Indexing has been changed from unti-clockwise to clockwise,
//  * thus Normal computation has to be changed too*/
//void terrainComputeNormals()
//{
//
//    int i, j;
//    glm::vec3 p1, p2, p3;
//    glm::vec3 normal;
//
//    /* allocate memory for terrainGridLength * terrainGridWidth elements */
//    for (int i = 0 ; i < terrainGridLength; i++)
//        for (int j = 0;j < terrainGridWidth; j++)
//            terrainNormals.push_back(glm::vec3(0.0f));
//
//    /* --- compute normals for points inside the grid --- */
//    for (int i = 1 ; i < terrainGridLength - 1; i++)
//        for (int j = 1;j < terrainGridWidth - 1; j++)
//        {
//            normal = glm::vec3(0.0f);
//
//            /* 1st triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//            p3 = terrainVertices[(i - 1) * terrainGridWidth + j];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* 2nd triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[i * terrainGridWidth + j - 1];
//            p3 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* 3rd triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[(i + 1) * terrainGridWidth + j];
//            p3 = terrainVertices[i * terrainGridWidth + j - 1];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* 4th triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[(i + 1) * terrainGridWidth + j + 1];
//            p3 = terrainVertices[(i + 1) * terrainGridWidth + j];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* 5th triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[i * terrainGridWidth + j + 1];
//            p3 = terrainVertices[(i + 1) * terrainGridWidth + j + 1];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* 6th triangle */
//            p1 = terrainVertices[i * terrainGridWidth + j];
//            p2 = terrainVertices[(i - 1) * terrainGridWidth + j];
//            p3 = terrainVertices[i * terrainGridWidth + j + 1];
//            normal += glm::cross(p3 - p1, p2 - p1);
//
//            /* normalize the normal */
//            normal = glm::normalize(normal);
//
//            /* assign normal to the vertex */
//            terrainNormals[i * terrainGridWidth + j] = normal;
//        }
//
//    /* --- compute normals for points on the edges of the grid --- */
//
//    /* --------  i == 0 && 0 < j < terrainGridWidth------- */
//    i = 0;
//    for (j = 1; j < terrainGridWidth - 1; j++)
//    {
//        normal = glm::vec3(0.0f);
//
//        /* 1st triangle */
//        p1 = terrainVertices[j];
//        p2 = terrainVertices[terrainGridWidth + j];
//        p3 = terrainVertices[j - 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 2nd triangle */
//        p1 = terrainVertices[j];
//        p2 = terrainVertices[terrainGridWidth + j + 1];
//        p3 = terrainVertices[terrainGridWidth + j];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 3rd triangle */
//        p1 = terrainVertices[j];
//        p2 = terrainVertices[j + 1];
//        p3 = terrainVertices[terrainGridWidth + j + 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//
//        /* normalize the normal */
//        normal = glm::normalize(normal);
//        /* assign normal to the vertex */
//        terrainNormals[j] = normal;
//    }
//
//    /* ----- i == terrainGridLength && 1 < j < terrainGridWidth - 1   -------- */
//    i = terrainGridLength;
//    for (j = 1; j < terrainGridWidth - 1; j++)
//    {
//        normal = glm::vec3(0.0f);
//
//        /* 1st triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//        p3 = terrainVertices[(i - 1) * terrainGridWidth + j];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 2nd triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[i * terrainGridWidth + j - 1];
//        p3 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 3rd triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[(i - 1) * terrainGridWidth + j];
//        p3 = terrainVertices[i * terrainGridWidth + j + 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* normalize the normal */
//        normal = glm::normalize(normal);
//        terrainNormals[i * terrainGridWidth + j] = normal;
//    }
//
//    /* --- compute normals for ( 0 < i < terrainGridWidth && j == 0) --- */
//    j = 0;
//    for (i = 1 ; i < terrainGridLength - 1; i++)
//    {
//        normal = glm::vec3(0.0f);
//
//        /* 1st triangle */
//        p1 = terrainVertices[i * terrainGridWidth];
//        p2 = terrainVertices[(i - 1) * terrainGridWidth];
//        p3 = terrainVertices[i * terrainGridWidth + 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 2nd triangle */
//        p1 = terrainVertices[i * terrainGridWidth];
//        p2 = terrainVertices[i * terrainGridWidth + 1];
//        p3 = terrainVertices[(i + 1) * terrainGridWidth + 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 3rd triangle */
//        p1 = terrainVertices[i * terrainGridWidth];
//        p2 = terrainVertices[(i + 1) * terrainGridWidth + 1];
//        p3 = terrainVertices[(i + 1) * terrainGridWidth];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* normalize the normal */
//        normal = glm::normalize(normal);
//        terrainNormals[i * terrainGridWidth + j] = normal;
//    }
//
//    /* --- compute normals for ( 0 < i < terrainGridWidth && j == terrainGridWidth - 1) --- */
//    j = terrainGridWidth - 1;
//    for (i = 1 ; i < terrainGridLength - 1; i++)
//    {
//        normal = glm::vec3(0.0f);
//
//        /* 1st triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//        p3 = terrainVertices[(i - 1) * terrainGridWidth + j];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 2nd triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[i * terrainGridWidth + j - 1];
//        p3 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* 3rd triangle */
//        p1 = terrainVertices[i * terrainGridWidth + j];
//        p2 = terrainVertices[(i + 1) * terrainGridWidth + j];
//        p3 = terrainVertices[i * terrainGridWidth + j - 1];
//        normal += glm::cross(p3 - p1, p2 - p1);
//
//        /* normalize the normal */
//        normal = glm::normalize(normal);
//        terrainNormals[i * terrainGridWidth + j] = normal;
//    }
//
//    /* --- compute normals for corners --- */
//
//    /* --- bottom left corner --- */
//    i = 0;
//    j = 0;
//    normal = glm::vec3(0.0f);
//
//    /* 1st triangle */
//    p1 = terrainVertices[0];
//    p2 = terrainVertices[1];
//    p3 = terrainVertices[terrainGridWidth + 1];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* 2nd triangle */
//    p1 = terrainVertices[0];
//    p2 = terrainVertices[terrainGridWidth + 1];
//    p3 = terrainVertices[terrainGridWidth];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* normalize the normal */
//    normal = glm::normalize(normal);
//    terrainNormals[i * terrainGridWidth + j] = normal;
//
//
//    /* --- bottom right corner --- */
//    i = 0;
//    j = terrainGridWidth - 1;
//    normal = glm::vec3(0.0f);
//
//    /* 1st triangle */
//    p1 = terrainVertices[j];
//    p2 = terrainVertices[terrainGridWidth + j];
//    p3 = terrainVertices[j - 1];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* normalize the normal */
//    normal = glm::normalize(normal);
//    terrainNormals[i * terrainGridWidth + j] = normal;
//
//    /* --- top left corner --- */
//    i = terrainGridLength - 1;
//    j = 0;
//    normal = glm::vec3(0.0f);
//
//    /* 1st triangle */
//    p1 = terrainVertices[i * terrainGridWidth];
//    p2 = terrainVertices[(i - 1) * terrainGridWidth];
//    p3 = terrainVertices[i * terrainGridWidth + 1];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* normalize the normal */
//    normal = glm::normalize(normal);
//    terrainNormals[i * terrainGridWidth + j] = normal;
//
//    /* --- top right corner --- */
//    i = terrainGridLength - 1;
//    j = terrainGridWidth - 1;
//    normal = glm::vec3(0.0f);
//
//    /* 1st triangle */
//    p1 = terrainVertices[i * terrainGridWidth + j];
//    p2 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//    p3 = terrainVertices[(i - 1) * terrainGridWidth + j];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* 2nd triangle */
//    p1 = terrainVertices[i * terrainGridWidth + j];
//    p2 = terrainVertices[i * terrainGridWidth + j - 1];
//    p3 = terrainVertices[(i - 1) * terrainGridWidth + j - 1];
//    normal += glm::cross(p3 - p1, p2 - p1);
//
//    /* normalize the normal */
//    normal = glm::normalize(normal);
//    terrainNormals[i * terrainGridWidth + j] = normal;
//
//}

