//
//  shapeGenerator.hpp
//  Assignment02
//
//  Created by Blood Burger on 26/6/17.
//
//

#ifndef shapeGenerator_hpp
#define shapeGenerator_hpp


void generateCube(std::vector<float> & vertices,
                  std::vector<unsigned short> & indices,
                  std::vector<float> & colors);

bool generateTerrain(const char * imagepath,
                     std::vector<glm::vec3> & vertices,
                     std::vector<unsigned short> & indices,
                     std::vector<glm::vec3> colors,
                     float xWidth = 2.0f, float zWidth = 2.0f, float yMin = 0.0f, float yMax = 1.0f);

bool generateSimpleTerrain(std::vector<glm::vec3> & vertices,
                           std::vector<unsigned short> & indices,
                           std::vector<glm::vec3> colors,
                           float xWidth = 2.0f, float zWidth = 2.0f, float yMin = 0.0f, float yMax = 1.0f);

//Uint32 getpixel(SDL_Surface *surface, int x, int y);




#endif /* shapeGenerator_hpp */
