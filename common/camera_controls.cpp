#include <stdio.h>
#include <stdlib.h>

// Include GLFW
#include <glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in playground.cpp. This is a hack from the tutorials. Kinda should be avoided :/

//#define degreesToRadians(x) x*(3.141592f/180.0f)
#define GLM_FORCE_RADIANS // tell glm to use radians by default
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "camera_controls.hpp"



glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
	return ProjectionMatrix;
}


// Initial position : on +Z
glm::vec3 position = glm::vec3( 0, 0, 10 );
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// Initial vertical angle : none
float verticalAngle = 0.0f;
// Initial Field of View
float initialFoV = 60.0f;
float zNear = 0.1f;
float zFar = 20.0f;

float speed = 5.0f; // 5 units / second
float mouseSpeed = 0.003f;



void computeMatricesFromInputs(){

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
    
    //Get window height and width
    int winWidth, winHeight;
    glfwGetWindowSize(window, &winWidth, &winHeight);

	// Reset mouse position for next frame
	glfwSetCursorPos(window, winWidth/2, winHeight/2);

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(winWidth/2 - xpos );
	verticalAngle   += mouseSpeed * float(winHeight/2 - ypos );

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle), 
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);
	
	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f/2.0f), 
		0,
		cos(horizontalAngle - 3.14f/2.0f)
	);
	
	// Up vector
	glm::vec3 up = glm::cross( right, direction );

	// Move forward
	if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS ){
		position += direction * deltaTime * speed;
	}
	// Move backward
	if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS){
		position -= direction * deltaTime * speed;
	}
	// Move right
	if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS){
		position += right * deltaTime * speed;
	}
	// Move left
	if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS){
		position -= right * deltaTime * speed;
	}

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this, but so far it's disabled.

	// Projection matrix : 45� Field of View, window width/height ratio, display range : 0.1 unit <-> 100 units
//	ProjectionMatrix = glm::perspective(FoV, (float)winWidth / (float)winHeight, zNear, zFar);
    ProjectionMatrix = calculateProjectionMatrix(FoV, (float)winWidth / (float)winHeight, zNear, zFar);
	// Camera matrix
	ViewMatrix       = glm::lookAt(
								position,           // Camera is here
								position+direction, // and looks here : at the same position, plus "direction"
								up                  // Head is up (set to 0,-1,0 to look upside-down)
						   );

	// For the next frame, the "last time" will be "current time"
	lastTime = currentTime;
}

glm::mat4 calculateProjectionMatrix(float FoV, float aspectRatio, float zNear, float zFar)
{
    glm::mat4 projectionMatrix(1.0f);
    float top, bottom, right, left;
    
    top = zNear * glm::tan(FoV * 3.141592f / 360.0f);
    bottom = -top;
    right = top * aspectRatio;
    left = bottom * aspectRatio;
    
    projectionMatrix[0][0] = 2 * zNear / (right - left);
    projectionMatrix[1][1] = 2 * zNear / (top - bottom);
    projectionMatrix[2][2] = (-1) * (zFar + zNear) / (zFar - zNear);
    projectionMatrix[2][3] = -1;
    projectionMatrix[3][2] = (-2) * zFar * zNear / (zFar - zNear);
    projectionMatrix[3][3] = 0;
    
    return projectionMatrix;
}


glm::vec4 getCameraPosition()
{
	return glm::vec4(position, 1.0f);
}

