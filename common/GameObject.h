
#ifndef INC_3407ICT_ASSIGNMENT02_GAMEOBJECT_H
#define INC_3407ICT_ASSIGNMENT02_GAMEOBJECT_H


struct collisionSphere
{
    float radius;
    glm::vec3 * position;
    glm::vec3 * speed;
    glm::vec3 * rotationChange;
};

class GameObject {

private:
    /* Variables for GPU Buffers */
    GLuint programID;
    GLuint TextureID;
    GLuint vertexBufferID;
    GLuint uvBufferID;
    GLuint normalBufferID;
    GLuint tangentBufferID;     // For Normlal Mapping
    GLuint bitangentBufferID;   // For Normlal Mapping
    GLuint elementBufferID;
    unsigned short indices_size; // size of the array of indices


    // For Normlal Map Shading
    GLuint DiffuseTextureID;
    GLuint NormalTextureID;
    GLuint SpecularTextureID;


    /* Variables for World Model positioning */
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 eulerAngles = glm::vec3(0.0f, 0.0f, 0.0f); // need to use quaternions in the future!
    glm::vec3 scaleVector = glm::vec3(1.0f, 1.0f, 1.0f);

    /* Variables for GameObjects movements */
    glm::vec3 speed = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 rotationChange = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 acceleration = glm::vec3(0.0f, 0.0f, 0.0f);

    /* Variables for Collision */
    collisionSphere collisionSphereBody;

public:

    GameObject();

    GameObject(GLuint in_vertexBufferID, GLuint in_uvBufferID,
               GLuint in_normalBufferID, GLuint in_elementBufferID,
               unsigned short in_indices_size);

    GameObject(const std::vector<glm::vec3> & vertices,
               const std::vector<glm::vec2> & uvs,
               const std::vector<glm::vec3> & normals,
               const std::vector<unsigned short> indices);

    GameObject(const std::vector<glm::vec3> & vertices,
               const std::vector<glm::vec2> & uvs,
               const std::vector<glm::vec3> & normals,
               const std::vector<glm::vec3> & tangents,
               const std::vector<glm::vec3> & bitangents,
               const std::vector<unsigned short> & indices);


    ~GameObject(); // !!! make it later

    void useShader(GLuint shaderID);

    /* Setters */
    void setShader(GLuint in_programID);
    void setTexture(GLuint in_TextureID);
    void setPosition(glm::vec3 newPosition);
    void setOrientation(glm::vec3 newEulerAngles);
    void setScale(glm::vec3 newScaleVecor);
    void setSpeed(glm::vec3 newSpeed);
    void setRotationChange(glm::vec3 newRotationChange);


    // For Normal Mapping
    void setDiffuseTexture(GLuint in_DiffuseTextureID);
    void setNormalTexture(GLuint in_NormalTextureID);
    void setSpecularTexture(GLuint in_SpecularTextureID);

//    void set

    /* Getters */
    GLuint getVertexBufferID();
    GLuint getUVBufferID();
    GLuint getNormalBufferID();
    GLuint getElementBufferID();
    unsigned short getIndicesSize();


    glm::vec3 getPosition();
    glm::vec3 getOrientation();
    glm::vec3 getScale();
    glm::vec3 getSpeed();
    glm::vec3 getRotationChange();


    /* Send vertices, normals, uvs, indices to OpenGL
     * and set up references to these massives in GPU */
    void initializeToOpenGL(const std::vector<glm::vec3> & vertices,
                                        const std::vector<glm::vec2> & uvs,
                                        const std::vector<glm::vec3> & normals,
                                        const std::vector<unsigned short> & indices);

    void initializeToOpenGL(const std::vector<glm::vec3> & vertices,
                            const std::vector<glm::vec2> & uvs,
                            const std::vector<glm::vec3> & normals,
                            const std::vector<glm::vec3> &tangents,
                            const std::vector<glm::vec3> &bitangents,
                            const std::vector<unsigned short> & indices);


    void draw();
    void drawWithNormalMap();
//    void updateModel(glm::vec3 addToPosition,
//                     glm::vec3 addToAngles, glm::vec3 addToScale);

    /* UPDATE Functions */
    void updateModel(glm::vec3 newPosition,
                     glm::vec3 newEulerAngles, glm::vec3 newScaleVector);
    void updatePosition();
    void updateOrientation();
    void update();
    void updateWithNormals();

    /* Collision Detection */
    void activateCollisionSphere(float radius); // assign sphere or cuboid
    collisionSphere getCollisionSphere();

};


#endif //INC_3407ICT_ASSIGNMENT02_GAMEOBJECT_H
