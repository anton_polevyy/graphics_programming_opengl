
//// Include single header image loading library stb_image
//#define STB_IMAGE_IMPLEMENTATION
//#include "stb_image.h"


// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <stdint.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>



#include "shapeGenerator.hpp"
#include "texture.hpp"


void generateCube(std::vector<float> & vertices,
             std::vector<unsigned short> & indices,
             std::vector<float> & colors){
    
    //Specify the Vertices
    float cubeVertices[] = {
        // front
        -1.0, -1.0,  1.0,
        1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,
        // back
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
        -1.0,  1.0, -1.0,
    };
    
    //Specify the colors
    float cubeColors[] = {
        // front colors
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
        // back colors
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
    };
    
    //Specify the triangles using indices into the vertices array
    unsigned short cubeIndices[] = {
        // front
        0, 1, 2,
        2, 3, 0,
        // top
        1, 5, 6,
        6, 2, 1,
        // back
        7, 6, 5,
        5, 4, 7,
        // bottom
        4, 0, 3,
        3, 7, 4,
        // left
        4, 5, 1,
        1, 0, 4,
        // right
        3, 2, 6,
        6, 7, 3,
    };
    
    // Copy generated cubeVertices to the vector std::vector<float> vertices
    unsigned arraySize = sizeof(cubeVertices) / sizeof(float);
    copy(&cubeVertices[0], &cubeVertices[arraySize], back_inserter(vertices));
    
    // Copy generated cubeColors to the vector std::vector<float> colors
    arraySize = sizeof(cubeColors) / sizeof(float);
    copy(&cubeColors[0], &cubeColors[arraySize], back_inserter(colors));
    
    // Copy generated cubeIndices to the vector std::vector<unsigned short> indices
    arraySize = sizeof(cubeIndices) / sizeof(unsigned short);
    copy(&cubeIndices[0], &cubeIndices[arraySize], back_inserter(indices));
}

//bool generateTerrain(const char * imagepath,
//                     std::vector<float> & vertices,
//                     std::vector<unsigned short> & indices)
//{
////    // Create OpenGL texture using stb_image library for file reading
////    GLuint textureID = load_stb_image(imagepath);
////
////    glBindTexture(GL_TEXTURE_2D, textureID);
////    GLenum texture_width, texture_height, texture_format;
////
////    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, (GLint*)&texture_width);
////    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, (GLint*)&texture_height);
////    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, (GLint*)&texture_format);
////
////    unsigned char* texture_bytes = (unsigned char*)malloc(sizeof(unsigned char)*texture_width*texture_height * 3);
////
////    glGetTexImage(GL_TEXTURE_2D, 0, GL_BGR, GL_UNSIGNED_BYTE, texture_bytes);
////
////    printf("\n\n\nGot from textureID:\n %s", texture_bytes);
//
//
//
//
//    // load image, create array of pixels[r][g][b][r][g][b]...
//    int width, height, nrChannels;
//    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
//    unsigned char *data = stbi_load(imagepath, &width, &height, &nrChannels, 0);
//    if (data)
//    {
//
//        //        printf("Image width: %d, height: %d, Num of channels: %d", width, height, nrChannels);
//        //        printf("Data from image file: %s \n", data);
//    }
//    else
//    {
//        printf("%s could not be opened.\n", imagepath); getchar();
//        return 0;
//    }
//    stbi_image_free(data);
//}



bool generateSimpleTerrain(std::vector<glm::vec3> & vertices,
                     std::vector<unsigned short> & indices,
                     std::vector<glm::vec3> colors,
                     float xWidth, float zWidth, float yMin, float yMax)
{
    glm::vec3 color(1.0, 0.0, 0.0);
    glm::vec3 vertex;
    int width = 4, length = 3;
    float dX = xWidth / width;
    float dZ = zWidth / length;

    for (short int j = 0; j < length; j++)
    {
        for(short int i = 0; i < width; i++)
        {


            /* Assign pixel position (i, j) to X * dX and Z * dZ
             * and average color to Y axis */
            vertex.x = i * dX;
            vertex.z = j * dZ;
            vertex.y = (i + j)/(float)(length + width);
            vertices.push_back(vertex);

            /* Assign colors to color vector */
            colors.push_back(color);
        }
    }

    /* Make indices vector */
    int v1, v2, v3, v4, v5, v6;
    for (int j = 0; j < length - 1; j++)
    {
        for (int i = 0; i < width - 1; i++)
        {
            // 1st triangle in the quad
            v1 = j * width + i;
            v2 = j * width + i + 1;
            v3 = (j + 1) * width + i + 1;

            // 2nd triangle in the quad
            v4 = (j + 1) * width + i + 1;
            v5 = (j + 1) * width + i;
            v6 = j * width + i;

            // push them to the vector of indices
            indices.push_back(v1);
            indices.push_back(v2);
            indices.push_back(v3);
            indices.push_back(v4);
            indices.push_back(v5);
            indices.push_back(v6);
        }
    }
}


