

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "camera_controls.hpp"

#include "GameObject.h"



GameObject::GameObject(){
    /* Variables for World Model positioning */
    position = glm::vec3(0.0f, 0.0f, 0.0f);
    eulerAngles = glm::vec3(0.0f, 0.0f, 0.0f); // need to use quaternions in the future!
    scaleVector = glm::vec3(1.0f, 1.0f, 1.0f);

    /* Variables for GameObjects movements */
    speed = glm::vec3(0.0f, 0.0f, 0.0f);
    rotationChange = glm::vec3(0.0f, 0.0f, 0.0f);
    acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
}

GameObject::GameObject(GLuint in_vertexBufferID, GLuint in_uvBufferID,
            GLuint in_normalBufferID, GLuint in_elementBufferID,
            unsigned short in_indices_size)
{
    GameObject();

    vertexBufferID = in_vertexBufferID;
    uvBufferID = in_uvBufferID;
    normalBufferID = in_normalBufferID;
    elementBufferID = in_elementBufferID;
    indices_size = in_indices_size;

}

GameObject::GameObject(const std::vector<glm::vec3> & vertices,
                       const std::vector<glm::vec2> & uvs,
                       const std::vector<glm::vec3> & normals,
                       const std::vector<unsigned short> indices)
{
    GameObject();

    initializeToOpenGL(vertices, uvs, normals, indices);
}

GameObject::GameObject(const std::vector<glm::vec3> & vertices,
           const std::vector<glm::vec2> & uvs,
           const std::vector<glm::vec3> & normals,
           const std::vector<glm::vec3> & tangents,
           const std::vector<glm::vec3> & bitangents,
           const std::vector<unsigned short> & indices)
{
    GameObject();

    initializeToOpenGL(vertices, uvs, normals, tangents, bitangents, indices);

}



GameObject::~GameObject()
{

}


void GameObject::useShader(GLuint shaderID)
{
    programID = shaderID;
}


/*
 * ----------------------- Setters ------------------
 * */
void GameObject::setPosition(glm::vec3 newPosition)
{
    position = newPosition;
}

void GameObject::setOrientation(glm::vec3 newEulerAngles)
{
    eulerAngles = newEulerAngles;
}

void GameObject::setScale(glm::vec3 newScaleVecor)
{
    scaleVector = newScaleVecor;
}


void GameObject::setShader(GLuint in_programID)
{
    programID = in_programID;
}

void GameObject::setTexture(GLuint in_TextureID)
{
    TextureID = in_TextureID;
}

void GameObject::setSpeed(glm::vec3 newSpeed)
{
    speed = newSpeed;
}

void GameObject::setRotationChange(glm::vec3 newRotationChange)
{
    rotationChange = newRotationChange;
}




void GameObject::setDiffuseTexture(GLuint in_DiffuseTextureID)
{
    DiffuseTextureID = in_DiffuseTextureID;
}

void GameObject::setNormalTexture(GLuint in_NormalTextureID)
{
    NormalTextureID = in_NormalTextureID;
}

void GameObject::setSpecularTexture(GLuint newSpecularTextureID)
{

    GLuint SpecularTextureID = newSpecularTextureID;
}


/*
 * ----------------------- Getters ------------------
 * */
GLuint GameObject::getVertexBufferID()
{
    return vertexBufferID;
}

GLuint GameObject::getUVBufferID()
{
    return uvBufferID;
}

GLuint GameObject::getNormalBufferID()
{
    return normalBufferID;
}

GLuint GameObject::getElementBufferID()
{
    return elementBufferID;
}

unsigned short GameObject::getIndicesSize()
{
    return indices_size;
}



glm::vec3 GameObject::getPosition()
{
    return position;
}

glm::vec3 GameObject::getOrientation()
{
    return eulerAngles;
}

glm::vec3 GameObject::getScale()
{
    return scaleVector;
}

glm::vec3 GameObject::getSpeed()
{
    return speed;
}

glm::vec3 GameObject::getRotationChange()
{
    return rotationChange;
}


/*
 * ----------------------- initializeToOpenGL Function ------------------
 * */

void GameObject::initializeToOpenGL(const std::vector<glm::vec3> & vertices,
                        const std::vector<glm::vec2> & uvs,
                        const std::vector<glm::vec3> & normals,
                        const std::vector<unsigned short> & indices)
{
    /* Send data to OpenGL */
//    GLuint vertexBufferID;
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

//    GLuint uvBufferID;
    glGenBuffers(1, &uvBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, uvBufferID);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

//    GLuint normalBufferID;
    glGenBuffers(1, &normalBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

    // Generate a buffer for the indices as well
//    GLuint elementBufferID;
    glGenBuffers(1, &elementBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

    indices_size = indices.size();
};


void GameObject::initializeToOpenGL(const std::vector<glm::vec3> & vertices,
                        const std::vector<glm::vec2> & uvs,
                        const std::vector<glm::vec3> & normals,
                        const std::vector<glm::vec3> & tangents,
                        const std::vector<glm::vec3> & bitangents,
                        const std::vector<unsigned short> & indices)
{
    /* Send data to OpenGL */
//    GLuint vertexBufferID;
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

//    GLuint uvBufferID;
    glGenBuffers(1, &uvBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, uvBufferID);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

//    GLuint normalBufferID;
    glGenBuffers(1, &normalBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &tangentBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, tangentBufferID);
    glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);

    GLuint bitangentbuffer;
    glGenBuffers(1, &bitangentBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, bitangentBufferID);
    glBufferData(GL_ARRAY_BUFFER, bitangents.size() * sizeof(glm::vec3), &bitangents[0], GL_STATIC_DRAW);


    // Generate a buffer for the indices as well
//    GLuint elementBufferID;
    glGenBuffers(1, &elementBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

    indices_size = indices.size();

    //    printf("GameObject has been initialized...");
}


/*
 * ----------------------- Draw Function ------------------
 * */

void GameObject::draw()
{

//    printf("GameObj position = (%f, %f, %f)\n", position.x, position.y, position.z);

    // Use our shader
    glUseProgram(programID);

    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureUnif  = glGetUniformLocation(programID, "myTextureSampler");

    // Get a handle for our "MVP" uniform
    // These uniforms are being used specifically for the PhongShading shader thus they might be irrelevant for another programID
    GLuint MatrixID = glGetUniformLocation(programID, "MVP");
    GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
    GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

    glm::mat4 ProjectionMatrix = getProjectionMatrix();
    glm::mat4 ViewMatrix = getViewMatrix();

//    glm::mat4 ModelMatrix = glm::mat4(1.0);
//    ModelMatrix = glm::translate(ModelMatrix, position);
//    ModelMatrix = glm::rotate(ModelMatrix, rotationAngle, glm::vec3(0.0f, 1.0f, 0.0f));
//    ModelMatrix = glm::scale(ModelMatrix, scaleVecor);

    // Build the model matrix
    /* as long as we using Euler Angles we need to rotate Y first, then Z, then X */
    glm::mat4 RotationMatrix = glm::eulerAngleYXZ(eulerAngles.y, eulerAngles.x, eulerAngles.z);
    glm::mat4 TranslationMatrix = glm::translate(glm::mat4(), position); // A bit to the left
    glm::mat4 ScalingMatrix = glm::scale(glm::mat4(), scaleVector);
    glm::mat4 ModelMatrix = TranslationMatrix * RotationMatrix * ScalingMatrix;

    glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

    // Send our transformation to the currently bound shader,
    // in the "MVP" uniform
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
    glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
    glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);




    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, TextureID);
    // Set our "myTextureSampler" sampler to user Texture Unit 0
    glUniform1i(TextureUnif, 0);

    // 1st attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glVertexAttribPointer(
            0,                  // attribute
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
    );

    // 2nd attribute buffer : UVs
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvBufferID);
    glVertexAttribPointer(
            1,                                // attribute
            2,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // 3rd attribute buffer : normals
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
    glVertexAttribPointer(
            2,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferID);

    // Draw the triangles !
    glDrawElements(
            GL_TRIANGLES,      // mode
            indices_size,    // the size of elementBuffer
            GL_UNSIGNED_SHORT,   // type
            (void*)0           // element array buffer offset
    );


    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}











void GameObject::drawWithNormalMap()
{
    // Use our shader
    glUseProgram(programID);

    // Get a handle for our "MVP" uniform
    GLuint MatrixUniform = glGetUniformLocation(programID, "MVP");
    GLuint ViewMatrixUniform = glGetUniformLocation(programID, "V");
    GLuint ModelMatrixUniform = glGetUniformLocation(programID, "M");
    GLuint ModelView3x3MatrixUniform = glGetUniformLocation(programID, "MV3x3");


    // Get a handle for our "myTextureSampler" uniform
    GLuint DiffuseTextureUniform  = glGetUniformLocation(programID, "DiffuseTextureSampler");
    GLuint NormalTextureUniform  = glGetUniformLocation(programID, "NormalTextureSampler");
    GLuint SpecularTextureUniform  = glGetUniformLocation(programID, "SpecularTextureSampler");

    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureUnif  = glGetUniformLocation(programID, "myTextureSampler");

    // Compute the MVP matrix from keyboard and mouse input
    computeMatricesFromInputs();
    glm::mat4 ProjectionMatrix = getProjectionMatrix();
    glm::mat4 ViewMatrix = getViewMatrix();
    glm::mat4 ModelMatrix = glm::mat4(1.0);
    glm::mat4 ModelViewMatrix = ViewMatrix * ModelMatrix;
    glm::mat3 ModelView3x3Matrix = glm::mat3(ModelViewMatrix);
    glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

    // Send our transformation to the currently bound shader,
    // in the "MVP" uniform
    glUniformMatrix4fv(MatrixUniform, 1, GL_FALSE, &MVP[0][0]);
    glUniformMatrix4fv(ModelMatrixUniform, 1, GL_FALSE, &ModelMatrix[0][0]);
    glUniformMatrix4fv(ViewMatrixUniform, 1, GL_FALSE, &ViewMatrix[0][0]);
    glUniformMatrix4fv(ViewMatrixUniform, 1, GL_FALSE, &ViewMatrix[0][0]);
    glUniformMatrix3fv(ModelView3x3MatrixUniform, 1, GL_FALSE, &ModelView3x3Matrix[0][0]);


    // Bind our diffuse texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, DiffuseTextureID);
    // Set our "DiffuseTextureSampler" sampler to user Texture Unit 0
    glUniform1i(DiffuseTextureUniform, 0);

    // Bind our normal texture in Texture Unit 1
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, NormalTextureID);
    // Set our "Normal	TextureSampler" sampler to user Texture Unit 0
    glUniform1i(NormalTextureUniform, 1);

    // Bind our normal texture in Texture Unit 2
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, SpecularTextureID);
    // Set our "Normal	TextureSampler" sampler to user Texture Unit 0
    glUniform1i(SpecularTextureUniform, 2);


    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glVertexAttribPointer(
            0,                  // attribute
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
    );

    // 2nd attribute buffer : UVs
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvBufferID);
    glVertexAttribPointer(
            1,                                // attribute
            2,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // 3rd attribute buffer : normals
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
    glVertexAttribPointer(
            2,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // 4th attribute buffer : tangents
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, tangentBufferID);
    glVertexAttribPointer(
            3,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // 5th attribute buffer : bitangents
    glEnableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, bitangentBufferID);
    glVertexAttribPointer(
            4,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
    );

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferID);

    // Draw the triangles !
    glDrawElements(
            GL_TRIANGLES,      // mode
            indices_size,    // count
            GL_UNSIGNED_SHORT, // type
            (void*)0           // element array buffer offset
    );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(4);

}


/*
// * ----------------------- Update Functions ------------------
// * */

void GameObject::updateModel(glm::vec3 newPosition,
                             glm::vec3 newEulerAngles, glm::vec3 newScaleVector)
{
    setPosition(newPosition);
    setOrientation(newEulerAngles);
    setScale(newScaleVector);
}

void GameObject::updatePosition()
{
    position = position + speed;
}
void GameObject::updateOrientation()
{
    eulerAngles = eulerAngles + rotationChange;
}

void GameObject::update()
{
    updatePosition();
    updateOrientation();
    draw();
}

void GameObject::updateWithNormals()
{
    updatePosition();
    updateOrientation();
    drawWithNormalMap();
}



/*
 * ------------------------- Collision Detection -----------------
 * */
void GameObject::activateCollisionSphere(float radius)
{
    collisionSphereBody.radius = radius;
    collisionSphereBody.position = &position;
    collisionSphereBody.speed = &speed;
}

collisionSphere GameObject::getCollisionSphere()
{
//    collisionSphereBody.position = position;
//    printf("collisionSphereBody.position = (%f, %f, %f)   \n ", position.x, position.y, position.z);

    return collisionSphereBody;
}
