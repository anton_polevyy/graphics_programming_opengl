//
// Created by Blood Burger on 3/7/17.
//

#ifndef GRAPHICS_PROGRAMMING_ASSIGNMENT_TERRAIN_H
#define GRAPHICS_PROGRAMMING_ASSIGNMENT_TERRAIN_H


#define TERRAIN_ERROR_INVALID_PARAM		-5
#define TERRAIN_ERROR_LOADING_IMAGE		-4
#define TERRAIN_ERROR_MEMORY_PROBLEM	-3
#define	TERRAIN_ERROR_NOT_SAVED			-2
#define TERRAIN_ERROR_NOT_INITIALISED	-1
#define TERRAIN_OK						 0


int generateTerrainFromTGA(char * imagepath, std::vector<glm::vec3> & vertices,
                       std::vector<glm::vec2> & uvs,
                       std::vector<glm::vec3> & normals,
                       std::vector<unsigned short> & indices);
int getHeightsFromTGA(char * imagepath);
void terrainComputeVertices(float xFrom, float xTo, float zFrom, float zTo, float yFrom, float yTo);
void terrainComputeIndices();
void terrainComputeNormals();
void terrainComputeUVs();
void terrainComputeUVsAsTiles();


#endif //GRAPHICS_PROGRAMMING_ASSIGNMENT_TERRAIN_H
