
#ifndef GRAPHICS_PROGRAMMING_ASSIGNMENT_BSP_TREE_H
#define GRAPHICS_PROGRAMMING_ASSIGNMENT_BSP_TREE_H


#define IN_FRONT    1
#define IN_BACK     2
#define INTERSECTS  3
#define SAME_LINE   4
#define ON_PLANE    5

#define EPSILON   0.00002




struct BSPnode {
    BSPnode * parent;
    BSPnode * front_child;
    BSPnode * back_child;
    glm::vec2 vertex1, vertex2;         //- end points of plane
//    face left_face, right_face;   // left is back, right is front
    glm::vec2 front_normal, back_normal;    // normal vectors from the segment (vertex2 - vertex1)
    glm::vec4 bounding_box;             //- bounds of subtree
    bool terminal_node;                 //- is this a leaf?
//    face terminal_node_faces[lots];     //- list of polygons
};

struct box {
    glm::vec2 left_positive;
    glm::vec2 left_negative;
    glm::vec2 right_positive;
    glm::vec2 right_negative;
};



//glm::vec3 playerPos;    //- player's map position
//glm::vec3 left_sightline; //- left-most screen pixel
//glm::vec3 right_sightline; //-right-most screen pixel


/*
 * ProjectionMatrix[0][0] = 2 * zNear / (right - left) is the right and left side clipping border
 * also Proj[0][0] = 1 / tan (Theta) = Adjacent / Opposite,
 * where Opposite is X axis and Adjacent is Z axis in ModelView coordinates
 * x' = ModelView * x
 * z' = ModelView * z
 *
 * thus z' = x' * Proj[0][0]
 *
 * */
glm::vec4 getLeftSightVector();
glm::vec4 getRightSightVector();




BSPnode * buildBSPTree(const std::vector<glm::vec2> & list_of_walls);

void insert(BSPnode * parentNode, glm::vec2 p1, glm::vec2 p2);
void insert(glm::vec2 p1, glm::vec2 p2);

/* gives back normalized normal 2D vector */
glm::vec2 countFrontNormal(glm::vec2 v1, glm::vec2 v2);
glm::vec2 countBackNormal(glm::vec2 v1, glm::vec2 v2);


/* check if the wall IN_FRONT, IN_BACK, INTERSECTS or at the SAME_LINE
 * with the parent's infinitely extended wall */
int checkRelation(BSPnode *leaf, glm::vec2 p1, glm::vec2 p2);

void splitWall(BSPnode * leaf, glm::vec2 & p1, glm::vec2 & p2,
               glm::vec2 & front_p1, glm::vec2 & front_p2,
               glm::vec2 & back_p1, glm::vec2 & back_p2);



/* ----------------------- Traversing Back BSP Tree ----------------------  */
void renderBSP(BSPnode *node, GLuint textureID, GLuint programID);
/* Draw Walls */
void drawFrontWall(glm::vec2 p1, glm::vec2 p2, glm::vec2 normal, GLuint TextureID, GLuint programID);
void drawBackWall(glm::vec2 p1, glm::vec2 p2, glm::vec2 normal, GLuint TextureID, GLuint programID);
/*  Check if point P is IN_FRONT, IN_BACK or ON_PLANE of the leaf plane */
int checkRelation(BSPnode *leaf, glm::vec4 P);




#endif //GRAPHICS_PROGRAMMING_ASSIGNMENT_BSP_TREE_H
