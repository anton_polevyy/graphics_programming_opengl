
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
//// glm::value_ptr
//#include <glm/gtc/type_ptr.hpp>



#include "common/shader.hpp"
#include "common/shader_s.h"
#include "common/texture.hpp"
#include "common/camera_controls.hpp"
#include "common/objloader.hpp"
#include "common/vboindexer.hpp"
#include "common/shapeGenerator.hpp"
#include "common/GameObject.h"
#include "common/terrain.h"
#include "common/tangentspace.hpp"
#include "common/bsp_tree.h"

#define FRAME_WIDTH     1024
#define FRAME_HEIGHT	768


bool checkSphereCollisions(collisionSphere sphere1, collisionSphere sphere2);
void checkCollisions();


// To Read .obj files
bool res;
std::vector<glm::vec3> vertices;
std::vector<glm::vec2> uvs;
std::vector<glm::vec3> normals;
std::vector<glm::vec3> tangents;
std::vector<glm::vec3> bitangents;

std::vector<unsigned short> indices;
std::vector<glm::vec3> indexed_vertices;
std::vector<glm::vec2> indexed_uvs;
std::vector<glm::vec3> indexed_normals;
std::vector<glm::vec3> indexed_tangents;
std::vector<glm::vec3> indexed_bitangents;



// Object movement variables
glm::vec3 objPosition = glm::vec3(0.0f, 0.0f, 0.0f);
// Circle movement variables
float x_center = 0.0f, y_center = 0.0f, radius = 4.0f, angle = 0.0f;
float rotAngle = 0.00f;
glm::vec3 eulerAngles = glm::vec3(0.0f, 0.0f, 0.0f);
// Scaling variables
glm::vec3 scaleVec = glm::vec3(1.0, 1.0, 1.0);



// For Collision Detection
//Collisions collisions = Collisions();
std::vector<collisionSphere> colSphereList;
float colSphereRadius = 1.0f;


// Walls for BSP Tree
std::vector<glm::vec2> list_of_walls = {
        glm::vec2(-2.0, -3.0), glm::vec2(0.0, -4.0),
        glm::vec2(-3.0, -5.0), glm::vec2(-2.0, -3.0),
        glm::vec2(-5.0, -1.0), glm::vec2(-3.0, -5.0),
        glm::vec2(0.0, -4.0), glm::vec2(-2.0, -6.0),
        glm::vec2(-2.0, -6.0), glm::vec2(4.0, -6.0),
        glm::vec2(4.0, -6.0), glm::vec2(4.0, -2.0),
        glm::vec2(4.0, -2.0), glm::vec2(2.0, -1.0),
        glm::vec2(2.0, -1.0), glm::vec2(2.0, 2.0),
        glm::vec2(2.0, 2.0), glm::vec2(4.0, 4.0)
};



int main( void )
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( FRAME_WIDTH, FRAME_HEIGHT, "Assignment02", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version.\n" );
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, FRAME_WIDTH/2, FRAME_HEIGHT/2);

    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.2f, 0.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);
//    glCullFace(GL_FRONT);
    glFrontFace(GL_CCW);
//    glCullFace(GL_BACK);
//    glFrontFace(GL_CW);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);





    // ------------- Build BSP Tree for Walls ------------------ ///

    BSPnode * root;

    root = buildBSPTree(list_of_walls);

    // Load the texture
    GLuint wallTextureID = load_stb_image("images/container.jpg");


    // ------------- End of BSP Tree for Walls Initialization ------------------ ///




    // ---- Initializing Phong Shader and passing it to OpenGL ---- ///

    // Create and compile our GLSL program from the shaders
    GLuint programID = LoadShaders( "shaders/PhongShading.vertexshader", "shaders/PhongShading.fragmentshader" );


    // ---- Initializing Object "Suzanne" and passing it to OpenGL ---- ///

    // Load the texture
    GLuint TextureID = loadDDS("obj_files/suzanne_uvmap.DDS");
//    GLuint TextureID = load_stb_image("images/container.jpg");

    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureUnif  = glGetUniformLocation(programID, "myTextureSampler");

    // Read our .obj file
    res = loadOBJ("obj_files/suzanne.obj", vertices, uvs, normals);

//    indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

    computeTangentBasis(
            vertices, uvs, normals, // input
            tangents, bitangents    // output
    );

    indexVBO_TBN(
            vertices, uvs, normals, tangents, bitangents,
            indices, indexed_vertices, indexed_uvs, indexed_normals, indexed_tangents, indexed_bitangents
    );




    // ---- Initialize GameObject "monkeyHead1" and pass it to OpenGL ---- ///

    GameObject monkeyHead1 = GameObject(indexed_vertices, indexed_uvs, indexed_normals, indices);
    monkeyHead1.setShader(programID);
    monkeyHead1.setTexture(TextureID);

    // add collision body
    monkeyHead1.activateCollisionSphere(colSphereRadius);
    colSphereList.push_back(monkeyHead1.getCollisionSphere());
//    collisions.addCollisionSphere(monkeyHead1.getCollisionSphere());

    // ---- End of GameObject "Suzanne" Initialization  ---- ///





    // ---- Initialize GameObject "monkeyHead2" and use monkeyHead1 data from OpenGL ---- ///

    GLuint monkeyVertexBufferID = monkeyHead1.getVertexBufferID();
    GLuint monkeyUVBufferID = monkeyHead1.getUVBufferID();
    GLuint monkeyNormalBufferID = monkeyHead1.getNormalBufferID();
    GLuint monkeyElementBufferID = monkeyHead1.getElementBufferID();
    unsigned short monkeyIndicesSize = monkeyHead1.getIndicesSize();

    GameObject monkeyHead2 = GameObject(monkeyVertexBufferID, monkeyUVBufferID,
                                        monkeyNormalBufferID, monkeyElementBufferID,
                                        monkeyIndicesSize);
    monkeyHead2.setShader(programID);
    monkeyHead2.setTexture(TextureID);

    // add collision body
    monkeyHead2.activateCollisionSphere(colSphereRadius);
    colSphereList.push_back(monkeyHead2.getCollisionSphere());
    monkeyHead2.setPosition(glm::vec3(-5.0f, 3.0f, 0.0f));
    monkeyHead2.setSpeed(glm::vec3(0.1f, 0.0f, 0.0f));

    // ---- End of GameObject "monkeyHead2" Initialization  ---- ///





    // ---- Initialize GameObject "monkeyHead3" and use monkeyHead1 data from OpenGL ---- ///

    GameObject monkeyHead3 = GameObject(monkeyVertexBufferID, monkeyUVBufferID,
                                        monkeyNormalBufferID, monkeyElementBufferID,
                                        monkeyIndicesSize);
    monkeyHead3.setShader(programID);
    monkeyHead3.setTexture(TextureID);

    // add collision body
    monkeyHead3.activateCollisionSphere(colSphereRadius);
    colSphereList.push_back(monkeyHead3.getCollisionSphere());
    monkeyHead3.setPosition(glm::vec3(3.5f, 3.0f, 3.5f));
    monkeyHead3.setSpeed(glm::vec3(-0.05f, 0.0f, -0.05f));

    // ---- End of GameObject "monkeyHead2" Initialization  ---- ///







    // ---- Initialize Terrain and pass it to OpenGL ---- ///

    // Load the texture
    GLuint terrainTextureID = load_stb_image("obj_files/grass_512x512.png");

    std::vector<glm::vec3> terrainVertices;
    std::vector<glm::vec2> terrainUVs;
    std::vector<glm::vec3> terrainNormals;
    std::vector<unsigned short> terrainIndices;

    generateTerrainFromTGA("images/heightMap_clouds.tga", terrainVertices,
                           terrainUVs, terrainNormals, terrainIndices);

    GameObject terrainFromTGA = GameObject(terrainVertices, terrainUVs, terrainNormals, terrainIndices);
    terrainFromTGA.setShader(programID);
    terrainFromTGA.setTexture(terrainTextureID);

    terrainVertices.clear();
    terrainUVs.clear();
    terrainNormals.clear();
    terrainIndices.clear();

    // ---- End of Terrain Initialization  ---- ///





    // ---- Set Up the light for Phong Shader ---- ///

    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);
    GLint LightUniform = glGetUniformLocation(programID, "LightPosition_worldspace");
    GLint LightColorUniform = glGetUniformLocation(programID, "LightColor");
    GLint LightPowerUniform = glGetUniformLocation(programID, "LightPower");

    // In this case the light is static, thus we can set it up before the main loop
    // Set up Light position
    glm::vec3 lightPos = glm::vec3(4,4,4);
    glUniform3f(LightUniform, lightPos.x, lightPos.y, lightPos.z);
    // Set up Light color and power
    glm::vec3 lightClr = glm::vec3(1.0,1.0,1.0);
    glUniform3f(LightColorUniform, lightClr.x, lightClr.y, lightClr.z);
    float lightPwr = 50.0f;
    glUniform1f(LightPowerUniform, lightPwr);







    // ---- Initialize monkeyHead with Normal Mapping ---- ///

    // ---- Initialize GameObject "monkeyHeadWithNormals" and use monkeyHead1 data from OpenGL ---- ///

    // Create and compile our GLSL program from the shaders
    GLuint normalProgramID = LoadShaders("shaders/NormalMapping.vertexshader", "shaders/NormalMapping.fragmentshader" );

    // Load the texture
//    GLuint DiffuseTexture = loadDDS("obj_files/diffuse.DDS");
    GLuint DiffuseTexture = loadDDS("obj_files/suzanne_uvmap.DDS");
    GLuint NormalTexture = loadBMP_custom("obj_files/normal.bmp");
    GLuint SpecularTexture = loadDDS("obj_files/suzanne_uvmap.DDS");

    GameObject monkeyHeadwithNormals(indexed_vertices, indexed_uvs, indexed_normals,
                                     indexed_tangents, indexed_bitangents, indices);
    monkeyHeadwithNormals.setShader(normalProgramID);
    monkeyHeadwithNormals.setDiffuseTexture(DiffuseTexture);
    monkeyHeadwithNormals.setNormalTexture(NormalTexture);
    monkeyHeadwithNormals.setSpecularTexture(SpecularTexture);

//     ---- End of GameObject "monkeyHeadWithNormals" Initialization  ---- ///



    //    For Normal Mapping
    // Get a handler for our "LightPosition" uniform
    glUseProgram(normalProgramID);
    GLuint normalLightUniform = glGetUniformLocation(normalProgramID, "LightPosition_worldspace");
    // Send Light position to the Normal Mapping
    glUniform3f(normalLightUniform, lightPos.x, lightPos.y, lightPos.z);



    // Free up vectors as all the data in GPU already and we don't need them anymore
    vertices.clear();
    uvs.clear();
    normals.clear();
    tangents.clear();
    bitangents.clear();
    indices.clear();
    indexed_vertices.clear();
    indexed_uvs.clear();
    indexed_normals.clear();
    indexed_tangents.clear();
    indexed_bitangents.clear();


    do{

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



        // Compute the MVP matrix from keyboard and mouse input
        computeMatricesFromInputs();


        ////// Terrain rendering //////

        terrainFromTGA.update();


        ////// Wall rendering //////

        renderBSP(root, wallTextureID, programID);



        //////  monkeyHead with Normal Mapping rendering //////
        monkeyHeadwithNormals.drawWithNormalMap();


        ////// 2nd and 3rd monkeyHeads rendering //////

        monkeyHead2.update();

        monkeyHead3.update();



        ////// Check Collisions  ///////////////////////////////
        checkCollisions();




        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    glDeleteBuffers(1, &monkeyVertexBufferID);
    glDeleteBuffers(1, &monkeyUVBufferID);
    glDeleteBuffers(1, &monkeyNormalBufferID);
    glDeleteBuffers(1, &monkeyElementBufferID);
//    glDeleteBuffers(1, &vertexbuffer);
//    glDeleteBuffers(1, &uvbuffer);
//    glDeleteBuffers(1, &normalbuffer);
    glUseProgram(0); // switch to default (fixed function) pipeline, because it's not good to delete the program which is being used
    glDeleteProgram(programID);
//    glDeleteProgram(programBasicID);
    glDeleteTextures(1, &TextureID);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}



//////// ------------- Collision Detection Functions --------- ///////

bool checkSphereCollisions(collisionSphere sphere1, collisionSphere sphere2)
{

    glm::vec3 distVec;
    distVec.x = sphere1.position->x - sphere2.position->x;
    distVec.y = sphere1.position->y - sphere2.position->y;
    distVec.z = sphere1.position->z - sphere2.position->z;

    float distance = sqrt( pow(distVec.x, 2) + pow(distVec.y, 2) + pow(distVec.z, 2) );


    return (distance < sphere1.radius + sphere2.radius);
}

void checkCollisions()
{
    if (colSphereList.size() > 1)
    {
        int iMax = colSphereList.size()/2;
        int jMin = colSphereList.size()/2 + 1;

        for (int i = 0; i <= iMax; i++)
        {
            for (int j = jMin; j < colSphereList.size(); j++)
            {
                if (i != j)
                {
                    if (checkSphereCollisions(colSphereList[i], colSphereList[j])) {
                        printf("------------- collision detected ----------\n");

                        // Swap speed vector values
                        glm::vec3 tempVec = *colSphereList[i].speed;
                        *colSphereList[i].speed = *colSphereList[j].speed;
                        *colSphereList[j].speed = tempVec;
                    }
                }
            }
        }
    }
}

